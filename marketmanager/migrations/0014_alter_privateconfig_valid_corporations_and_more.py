# Generated by Django 4.0.3 on 2022-03-13 14:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketmanager', '0013_alter_privateconfig_failure_reason_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='privateconfig',
            name='valid_corporations',
            field=models.ManyToManyField(blank=True, to='eveonline.evecorporationinfo', verbose_name='Valid Corporation Markets for this Token'),
        ),
        migrations.AlterField(
            model_name='privateconfig',
            name='valid_structures',
            field=models.ManyToManyField(blank=True, to='marketmanager.structure', verbose_name='Valid Structure Markets for this Token'),
        ),
    ]
