# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-01 00:35+1000\n"
"PO-Revision-Date: 2021-11-30 14:39+0000\n"
"Language-Team: Korean (Korea) (https://www.transifex.com/alliance-auth/teams/107430/ko_KR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ko_KR\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: auth_hooks.py:16 templates/marketmanager/marketbrowser.html:4
msgid "Market Browser"
msgstr ""

#: auth_hooks.py:35
msgid "Market Manager"
msgstr ""

#: models.py:31
msgid "Fetch Regions"
msgstr ""

#: models.py:52
msgid "Order ID"
msgstr ""

#: models.py:57 models.py:173
msgid "Type"
msgstr ""

#: models.py:60
msgid "Duration"
msgstr ""

#: models.py:63 models.py:203
msgid "Buy Order"
msgstr ""

#: models.py:67
msgid "Issued"
msgstr ""

#: models.py:72
msgid "Location ID"
msgstr ""

#: models.py:76
msgid "System"
msgstr ""

#: models.py:82 models.py:104 models.py:212
#: templates/marketmanager/marketbrowser/buy_orders.html:11
#: templates/marketmanager/marketbrowser/sell_orders.html:11
msgid "Region"
msgstr ""

#: models.py:85
msgid "Minimum Volume"
msgstr ""

#: models.py:90 models.py:224
#: templates/marketmanager/marketbrowser/buy_orders.html:13
#: templates/marketmanager/marketbrowser/sell_orders.html:13
msgid "Price"
msgstr ""

#: models.py:95
msgid "Escrow"
msgstr ""

#: models.py:105 models.py:169 models.py:207
msgid "Solar System"
msgstr ""

#: models.py:106
msgid "Station"
msgstr ""

#: models.py:108
msgid "Order Range"
msgstr ""

#: models.py:113
msgid "Volume Remaining"
msgstr ""

#: models.py:116
msgid "Volume Total"
msgstr ""

#: models.py:119
msgid "Is Corporation"
msgstr ""

#: models.py:123
msgid "Wallet Division"
msgstr ""

#: models.py:129
msgid "Character"
msgstr ""

#: models.py:135
msgid "Corporation"
msgstr ""

#: models.py:141
msgid "Cancelled"
msgstr ""

#: models.py:142
msgid "Expired"
msgstr ""

#: models.py:144
msgid "Order State"
msgstr ""

#: models.py:161
msgid "Structure ID"
msgstr ""

#: models.py:164 models.py:189
msgid "Name"
msgstr ""

#: models.py:166
msgid "Owner ID"
msgstr ""

#: models.py:176
msgid "Pull Market Orders"
msgstr ""

#: models.py:191
msgid "URL"
msgstr ""

#: models.py:204
msgid "EVE Types"
msgstr ""

#: models.py:206
msgid "Structure"
msgstr ""

#: models.py:210
msgid "Constellation"
msgstr ""

#: models.py:217
msgid "Structure Type Filter"
msgstr ""

#: models.py:221
msgid "Volume"
msgstr ""

#: models.py:227
msgid "Jita Comparison %"
msgstr ""

#: models.py:232
msgid "Webhooks"
msgstr ""

#: templates/marketmanager/marketbrowser/buy_orders.html:5
msgid "Buy Orders"
msgstr ""

#: templates/marketmanager/marketbrowser/buy_orders.html:12
#: templates/marketmanager/marketbrowser/sell_orders.html:12
msgid "Quantity"
msgstr ""

#: templates/marketmanager/marketbrowser/buy_orders.html:14
#: templates/marketmanager/marketbrowser/sell_orders.html:14
msgid "Location"
msgstr ""

#: templates/marketmanager/marketbrowser/buy_orders.html:15
#: templates/marketmanager/marketbrowser/sell_orders.html:15
msgid "Expires"
msgstr ""

#: templates/marketmanager/marketbrowser/buy_orders.html:16
#: templates/marketmanager/marketbrowser/sell_orders.html:16
msgid "Last Updated"
msgstr ""

#: templates/marketmanager/marketbrowser/buy_orders.html:21
#: templates/marketmanager/marketbrowser/sell_orders.html:21
msgid "Owned Character Orders"
msgstr ""

#: templates/marketmanager/marketbrowser/buy_orders.html:24
#: templates/marketmanager/marketbrowser/sell_orders.html:24
msgid "Owned Corporation Orders"
msgstr ""

#: templates/marketmanager/marketbrowser/sell_orders.html:5
msgid "Sell Orders"
msgstr ""
