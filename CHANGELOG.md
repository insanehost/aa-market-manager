# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [] - yyyy-mm-dd
### Added
### Changed
### Fixed

## [v0.9.3a] - 2022-08-06
### Changed
- ManagedWatchConfig admin changed for more useful info
- jitacomparepercent will override flat prices as described in watchconfig model/form
### Fixed
- WatchConfig admin page loads again
- handle ManagedApp names nicer, `AA-Fittings` not `fittings`


## [v0.9.2a] - 2022-08-05
### Fixed
- only use typestatistics when jitacomparepercent is actually set None != 0
- added try except for typestatistics if configured incorrectly

## [v0.9.1a] - 2022-08-03
### Fixed
- WatchConfigs from ManagedWatchConfigs now include the Hulls
- Jita compare % should work?
- WatchConfig discord outputs now only include price when relevant, and include formatted data when using jita %
## [v0.9.0a] - 2022-08-02
### Added
- AA-Discordbot channel messages as a destination for alerts, in addition to already provided webhooks

## [v0.8.2a] - 2022-08-02
### Added
### Changed
### Fixed
- typo when dealing with managed watch configs in discord alerts

## [v0.8.1a] - 2022-08-01
### Fixed
- Bad import from Fittings module

## [v0.8.0a] - 2022-08-01
### Added
- Basic framework for maintaining WatchConfigs by External Applications.
- Initial Fittings support, a ManagedWatchConfig will create and update a WatchConfig for a given Fit
- AA3 and AA2 Support/testing

## [v0.6.7] - 2022-07-31
### Fixed
- Fix Gitlab CI

## [v0.6.6a] - 2022-07-31
### Fixed
- Corrected garbage collection to tidy up orders and structures, not structures twice
- Brought default stale order time down to 7 days from 30
- Loosened up py-cord requirements
## [v0.6.5a] - yyyy-mm-dd
### Fixed
- Now properly setting is_buy_order when this bool is optional (#ccp some consistency PLEASE), Character and Corporation orders will no longer all be Buy Orders.

## [v0.6.4a] - 2022-03-22
### Fixed
- Market Browser Autocomplete Item Search now completes on Enter, Does not output TypeIDs to browser. https://gitlab.com/tactical-supremacy/aa-market-manager/-/issues/16
- Market Browser Region selector now filters by valid Regions, Sorted Alphabetically https://gitlab.com/tactical-supremacy/aa-market-manager/-/issues/17
## [v0.6.3a] - 2022-03-21
### Fixed
- Test fixes
## [v0.6.2a] - 2022-03-20
### Fixed
- Corrected django-solo requirement to greater than or equal to.

## [v0.6.1a] - 2022-03-20
### Added
- Some basic Initial Tests, more to come
### Changed
- Public market fetching now happens in independent Buy/Sell tasks. This is slightly slower overall but results in shorter running tasks
- Removed an extra get call from each market order save

## [v0.6.0a] - 2022-03-19

### Added
- Locally calculated Medians, Percentiles and Weighted Averages, this is to avoid long calculations holding up page loads

### Changed
- DB Optimizations, DB Optimizations and DB Optimizations

### Fixed
- Volume column is now formatted nicely in Market Browser
- Explanatory footer added when TypeStatistics arent available.

## [v0.5.1a] - 2022-03-17
### Changed
- Added theming to the jQuery-UI elements that were coming up unthemed.
- Removed our own jQuery-UI, shipped with AA now
- Improved item icons and details pane

### Fixed
- Preload commands now properly respect the multi category inputs of django-eveuniverse
- Removed some unneeded divs from market browser

## [v0.5.0a] - 2022-03-15
### Added
- Location filtering to WatchConfigs
- PyCord 2.0.0b1 (to be in line with discordbot), is now a dependency
    - It is much easier to generate and Send Webhooks all from within pycord
    - This might be removed later depending on how much effort it works out to be.

### Changed
- Significant speedups to WatchConfig admin and other admin QOL
- Some framework for calculation market statistics
- Basic framework for outside apps to manage WatchConfigs

### Fixed
- Market orders in Upwell Structures now properly save their Solar System as well.
- Properly uses Error Warning Success webhook colours

## [v0.4.1a] - 2022-03-15
### Fixed
- Possibly fixed Migration issue with Squashes set as dependency
## [v0.4.0a] - 2022-03-15
### Added
- Supply Check WatchConfig.
    - Checks for configured volume at configured price
- Discord Webhooks
### Changed
- Updated documentation
    - Project Status and Features
    - Permissions
    - Settings
### Fixed
- Removed an AA 3.0 dependency that makemigrations included erroneously.
- Private Structure Orders are now properly detected as true/false for is_buy_order #ccp

## [v0.3.0a] - 2022-03-14
You might need to Truncate your Orders table here, I did weird things ymmv.

### Added
- Django 4 Support @soratidus999, @ppfeufer
- Private Structure order fetching
- Private Structure updating (private structures still need to be created by hand)
- Migration Squashed up until now, expect a full wipe on Stable release.
- Significantly better Admin views, still WIP
### Changed
- Indexes (maybe too many)
- Public order save optimizations, markedly faster at updating and bulk saving.
- Public Market data pullled/saved in pages, to prevent balooning memory
- Configs are now PrivateConfig and PublicConfig
### Fixed
- EveUniverse Pre-Load commands @ppfeufer

### Dev
- isort

## [v0.2.2a] - yyyy-mm-dd
### Added
- eveuniverse preload command
### Changed
- whole swatch of exception handling and logging, some debug logs

### Fixed
## [v0.2.1] - 2021-12-1

### Added
- Character and Corporation owned order highlighting
- Translation framework, added to the Alliance Auth Transifex Project
### Changed
- Shrunks a column size, looking for more trimming to do
### Fixed
- Simplified the order queries by half by using chained queries for region filtering
## [v0.2.0a] - 2021-11-29

### Added

### Changed
- DataTables now load with ajax functions removing their impact on page loads
- MarketGroup hierarchy now displayed for items
- MarketGroup dropdown (still WIP) only displays top level market groups, this is how it should always work and it was creating a massive dropdown behind the scenes before
- Permissions refactor and cleanup
### Fixed
- Station resolution for buy orders were fixed by not sell.

## [v0.1.4a] - 2021-11-28

### Added
- added DataTables saveState function to save its configuration to browser localstorage
### Changed
- EveRegions, EveStations and Structures now all bulk resolve cutting down page loads significantly
### Fixed
- alt= url sneaking in from missing image url when item not set
- corporation tasks were not exiting cleanly when no valid token was found to run said task
- autocomplete was returning items not available for sale on the market.
## [v0.1.3a] - yyyy-mm-dd

### Fixed
- included the swagger.json in the manifest, fixing pip installs

## [v0.1.2a] - yyyy-mm-dd

### Fixed
- The all characters task was calling corporation IDs, fixes fetch_all_character_orders
## [v0.1.0a] - 2021-11-26
### Fixed
- CeleryBeat configuration
- Templates not yet included in Alliance Auth, are now loaded in Market Manager and will be overriden if included by AA.

## [v0.1.0a] - 2021-11-26

### Initital Release
